/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxlab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OXLab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            
            if(isWin()) {
                printTable();
                printWin();
                break;
            }
            switchPlayer();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " " + "turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        
        while (true) {
            System.out.print("Please input row col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }

    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }

    }

    private static boolean isWin() {
        if(CheckRow()|| CheckCol()|| CheckX1() || CheckX2()) {
            return true;
            
        }
        
        return false ;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Win!!");
    }

    private static boolean CheckRow() {
        for(int i= 0 ;i<3 ; i++) {
            if(table[row-1][i]!= currentPlayer) return false ;
        }
        return true;
    }

    private static boolean CheckCol() {
        return false;
    }

    private static boolean CheckX1() {
        return false;
    }

    private static boolean CheckX2() {
        return false;
    }

}
